class gitolite(
  String $admin_key,
  Enum['file', 'absent'] $ensure    = 'file',
  String $gituser                   = 'git',
  String $path                      = "/var/lib/${gituser}",
  Boolean $deploy_gitoliterc        = true,
  Integer $version                  = 3,
  String $package                   = 'gitolite3',
  Boolean $backward_compat          = false,
) {

  $response_file = "/var/cache/debconf/${package}.preseed"

  case $::operatingsystem {
    'debian': {
      file { $response_file:
        ensure  => $ensure,
        content => template('gitolite/gitolite.preseed.erb'),
        before  => Package[$package],
      }
    }
    default: { fail "awfully sorry, but ${::operatingsystem} is not yet supported" }
  }

  package { $package:
    ensure       => present,
    responsefile => $response_file,
  }

  user { $gituser:
    allowdupe  => true,
    ensure     => present,
    home       => $path,
    managehome => false,
  }

  file { $path:
    mode    => '0750',
    owner   => $gituser,
    group   => $gituser,
    require => Package[$package],
  }

  file { "${path}/repositories":
    mode  => '0750',
    owner => $gituser,
    group => $gituser,
  }

  if $deploy_gitoliterc {
    if $version > 2 {
      fail "Sorry, deploying gitoliterc is not supported with Gitolite >2"
    }

    file { "${path}/.gitolite.rc":
      source => [ 'puppet:///modules/site_gitolite/gitolite.rc',
                  'puppet:///modules/gitolite/gitolite.rc' ],
      mode   => '0644',
      owner  => $gituser,
      group  => $gituser,
    }
  }

  file { '/usr/share/doc/git-core/contrib/hooks/post-receive-email':
    mode   => '0755',
    owner  => root,
    group  => root,
  }

  if $backward_compat {
    if $version == 3 {

      file { '/var/lib/gitolite':
        ensure => 'link',
        target => '/var/lib/gitolite3',
      }

      if $::gitolite3_uid and $::gitolite3_gid {
        if $::gitolite3_uid > 0 and $::gitolite3_gid > 0 {
          user { 'gitolite':
            allowdupe  => true,
            uid        => $::gitolite3_uid,
            gid        => $::gitolite3_gid,
            home       => '/var/lib/gitolite3',
            managehome => false,
            shell      => '/bin/bash',
          }
        }
        else {
          fail "refusing to make a fake root user, don't run gitolite as root!"
        }
      }
      else {
        fail "cannot determine gitolite3's uid, failing on backward compatibility"
      }

    }
    else {
      fail "Sorry, only backward compatibility for gitolite3 to 2 is supported"
    }

  }

  if $version == 2 {
    file { "${path}/.gitolite":
      ensure  => directory,
      mode    => '0700',
      owner   => $gituser,
      group   => $gituser,
      require => Package[$package],
      notify  => Exec['gl-setup'];
    }

    exec { 'gl-setup':
      command     => '/usr/bin/gl-setup',
      refreshonly => true;
    }
  }
}
