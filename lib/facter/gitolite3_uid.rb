require 'etc'

Facter.add('gitolite3_uid') do
  setcode do
    uid = Etc.getpwnam('nobody').uid
    Etc.passwd do |u|
      uid = u.uid if u.name == 'gitolite3'
    end
    uid
  end
end
