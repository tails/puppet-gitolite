require 'etc'

Facter.add('gitolite3_gid') do
  setcode do
    begin
      Etc.getpwnam('gitolite3').gid
    rescue
      Etc.getpwnam('nobody').gid
    end
  end
end
